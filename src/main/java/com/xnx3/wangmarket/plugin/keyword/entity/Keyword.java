package com.xnx3.wangmarket.plugin.keyword.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 文章关键词插件
 * @author 管雷鸣
 */
@Entity
@Table(name = "plugin_keyword")
public class Keyword implements java.io.Serializable {

	private Integer id;	
	private Integer siteid;	//对应站点id，site.id。是哪个站点使用这个规则。如果这里为0，则是全局的，针对所有站点都生效
	private String word;	//要替换的词
	
	public Keyword() {
		siteid = 0;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false )
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "siteid", columnDefinition="int(11) comment '对应站点id，site.id。是哪个站点使用这个规则。如果这里为0，则是全局的，针对所有站点都生效'")
	public Integer getSiteid() {
		return siteid;
	}

	public void setSiteid(Integer siteid) {
		this.siteid = siteid;
	}

	@Column(name = "word", columnDefinition="char(20) comment '要替换的词'")
	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}
	
	
}