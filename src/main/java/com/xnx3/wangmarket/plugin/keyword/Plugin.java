package com.xnx3.wangmarket.plugin.keyword;

import com.xnx3.j2ee.pluginManage.PluginRegister;

/**
 * 文章关键词插件
 * @author 管雷鸣
 */
@PluginRegister(menuTitle = "关键词替换", intro="网站中，文章修改后进行保存时，将内容自动进行关键词替换", menuHref="/plugin/keyword/siteadmin/list.do", applyToCMS=true, version="1.2", versionMin="5.5")
public class Plugin{
	
}