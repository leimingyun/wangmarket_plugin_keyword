package com.xnx3.wangmarket.plugin.keyword.service;

import java.util.List;

import com.xnx3.wangmarket.admin.entity.Site;
import com.xnx3.wangmarket.plugin.keyword.entity.Keyword;

/**
 * 关键字
 * @author 管雷鸣
 */
public interface KeywordService {
	
	/**
	 * 刷新超级管理后台所定义的关键字列表，将其最新的数据加入内存缓存
	 */
	public void refreshKeywordListCacheForSuperadmin();
	
	/**
	 * 获取当前登录用户网站的关键字列表。
	 * 先从session中取，若session中没有缓存，则从数据库中取。取出来放入session中缓存
	 */
	public List<Keyword> getKeywordList();
	
	/**
	 * 刷新当前登录用户网站的关键字列表。这里是重新查询数据库中的关键字列表，将其赋予session缓存
	 */
	public void refreshKeywordListCache(Site site);
}
