package com.xnx3.wangmarket.plugin.keyword.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.xnx3.j2ee.dao.SqlDAO;
import com.xnx3.j2ee.util.ConsoleUtil;
import com.xnx3.wangmarket.admin.Func;
import com.xnx3.wangmarket.admin.entity.Site;
import com.xnx3.wangmarket.admin.util.SessionUtil;
import com.xnx3.wangmarket.plugin.keyword.entity.Keyword;
import com.xnx3.wangmarket.plugin.keyword.service.KeywordService;

@Service("keywordService")
public class KeywordServiceImpl implements KeywordService {
	
	@Resource
	private SqlDAO sqlDAO;

	public void refreshKeywordListCacheForSuperadmin() {
		List<Keyword> list = sqlDAO.findBySqlQuery("SELECT * FROM plugin_keyword WHERE siteid = 0", Keyword.class);
	}

	public List<Keyword> getKeywordList() {
		List<Keyword> keywordList;	//要返回的关键字列表
		
		//session中缓存的关键字列表
		Object obj = Func.getUserBeanForShiroSession().getPluginDataMap().get("keyword");
		Site site = SessionUtil.getSite();
		
		if(site == null){
			return new ArrayList<Keyword>();
		}
		
		int siteid = site.getId();
		if(obj == null){
			//没有，那么从数据库中取
			keywordList = sqlDAO.findByProperty(Keyword.class, "siteid", siteid);
			//将查询出来的关键字列表，缓存入 session 中，以便再编辑其他文章
			Func.getUserBeanForShiroSession().getPluginDataMap().put("keyword", keywordList);
		}else{
			//缓存中有，那么从缓存中取
			keywordList = (List<Keyword>) obj;
		}
		
		return keywordList;
	}

	@Override
	public void refreshKeywordListCache(Site siteOriginal) {
		List<Keyword> keywordList = sqlDAO.findByProperty(Keyword.class, "siteid", siteOriginal.getId());
		//将查询出来的关键字列表，缓存入 session 中，以便再编辑其他文章
		Func.getUserBeanForShiroSession().getPluginDataMap().put("keyword", keywordList);
		
		String keywords = "";
		for (int i = 0; i<keywordList.size(); i++) {
			if(keywordList.get(i).getWord().trim().length() == 0) {
				continue;
			}
			if(keywords.length() > 0) {
				keywords = keywords + ",";
			}
			keywords = keywords + keywordList.get(i).getWord().trim();
		}
		ConsoleUtil.log("keywords:"+keywords);
		Site site = sqlDAO.findById(Site.class, siteOriginal.getId());
		site.setKeywords(keywords);
		sqlDAO.save(site);
		
		//更新session
		SessionUtil.setSite(site);
	}

	
}
