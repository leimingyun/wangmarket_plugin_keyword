<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<jsp:include page="../../../iw/common/head.jsp">
	<jsp:param name="title" value="编辑系统参数"/>
</jsp:include>

<form class="layui-form" style="padding-top:35px; margin-bottom: 0px; padding-right:35px; background: #FFFFFF;">
	<input type="hidden" name="id" value="${keyword.id }" />
	<div class="layui-form-item">
		<label class="layui-form-label" id="label_columnName">关键字、词</label>
		<div class="layui-input-block">
			<input type="text" name="word"  required lay-verify="required" autocomplete="off" placeholder="只限汉字、英文、数字，不可有符号" class="layui-input" value="${keyword.word }">
		</div>
	</div>
	
   	<div class="layui-form-item" style="padding-top:15px; margin-bottom: 0px;">
		<div class="layui-input-block">
			<button class="layui-btn" lay-submit="" lay-filter="demo1" style=" margin-bottom: 20px;">保存修改</button>
		</div>
	</div>
</form>

<script>
//自适应弹出层大小
var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
parent.layer.iframeAuto(index);

layui.use(['form', 'layedit', 'laydate'], function(){
  var form = layui.form;
  
  //监听提交
  form.on('submit(demo1)', function(data){
  	  	parent.parent.msg.loading("保存中");
		var d=$("form").serialize();
        $.post("save.do", d, function (result) { 
        	parent.parent.msg.close();
        	var obj = JSON.parse(result);
        	if(obj.result == '1'){
        		parent.parent.msg.success("操作成功")
        		parent.layer.close(index);
        		parent.location.reload();	//刷新父窗口
        	}else if(obj.result == '0'){
        		parent.layer.msg(obj.info, {shade: 0.3})
        	}else{
        		parent.layer.msg(result, {shade: 0.3})
        	}
         }, "text");
		
    return false;
  });
  
});  
</script>

<jsp:include page="../../../iw/common/foot.jsp"></jsp:include> 