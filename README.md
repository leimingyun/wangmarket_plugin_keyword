
## 功能介绍
[wangmarket（网市场云建站系统）](https://gitee.com/mail_osc/wangmarket) 的 关键字替换插件。  

内容管理中，编写的文章正文内容 (也就是模板中 {news.text} 所调出来的内容 )，如果其中有与之匹配的关键字，在生成网站时会自动给这些关键词加上超链接。  
加的超链接也就是当前网站在系统设置-绑定域名中，自己为网站绑定的域名。比如这里绑定的域名，也就是超链接为： http://www.guanleiming.com  
文章中有某段内容为  

````
为农作物喷洒农药可有效降低病虫害
````

生成整站后，会变为 :

````
为农作物喷洒<a href="http://www.guanleiming.com" class="keyword">农药<a>可有效降低病虫害
````
  

## 使用条件
1. 本项目(生成的 target/wangmarket-plugin-xxx.jar)放到 [网市场云建站系统](https://gitee.com/leimingyun/wangmarket_deploy) 中才可运行使用
1. 网市场云建站系统本身需要 v5.6 或以上版本。
1. 本项目只支持 mysql 数据库。使用默认 sqlite 数据库的不可用

## 使用方式
1. 下载 /target/ 中的jar包
1. 将下载的jar包放到 tomcat/webapps/ROOT/WEB-INF/lib/ 下
1. 重新启动运行项目，登陆网站管理后台，即可看到左侧的 功能插件 下多了 关键字替换 功能。


## 二次开发
#### 本插件的二次开发
1. 运行项目，随便登陆个网站管理后台（默认运行起来后，会自带一个默认账号密码都是 wangzhan 的账号，可以用此账号登陆）
1. 登陆网站管理后台后，找到左侧的 功能插件 下的 关键字替换 ，即可。

#### 从头开始开发一个自己的插件
参考文档：  https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=3213258&doc_id=1101390

#### 二次开发wangmarket及功能的扩展定制
可参考：  https://gitee.com/leimingyun/wangmarket_deploy

## 合作洽谈
作者：管雷鸣<br/>
微信：xnx3com<br/>
QQ：921153866<br/>



